package com.example.tbcshualedur.pageresources

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.tbcshualedur.api.RetrofitService
import com.example.tbcshualedur.model.news.Article

class SearchNewsSource(private val searchQuery: String) : PagingSource<Int, Article>() {
    override fun getRefreshKey(state: PagingState<Int, Article>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Article> {
        try {
            val currentPage = params.key ?: 1
            val newsResponse = RetrofitService.newsApi.searchNews(searchQuery, currentPage)
            val responseData = mutableListOf<Article>()
            val data = newsResponse.body()?.articles ?: emptyList()
            Log.d("TAG", "$data")
            responseData.addAll(data)

            val prevKey = if (currentPage == 1) null else currentPage - 1
            val nextKey = if (data.isEmpty()) null else currentPage + 1
            return LoadResult.Page(
                responseData, prevKey, nextKey
            )
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }
}