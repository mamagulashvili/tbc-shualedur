package com.example.tbcshualedur.util

object Constants {

    const val BASE_URL = "https://newsapi.org/"
    const val API_KEY = "3b11e4c22d3944aa90248b65699f9b9f"
    const val COUNTRY_CODE = "gb"

    const val DATABASE_NAME = "news_database"
}