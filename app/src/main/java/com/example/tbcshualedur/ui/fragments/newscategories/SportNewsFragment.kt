package com.example.tbcshualedur.ui.fragments.newscategories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tbcshualedur.adapter.NewsAdapter
import com.example.tbcshualedur.databinding.FragmentSportNewsBinding
import com.example.tbcshualedur.ui.fragments.news.NewsFragmentDirections
import com.example.tbcshualedur.ui.fragments.news.NewsViewModel
import com.example.tbcshualedur.ui.loadSateListener

class SportNewsFragment : Fragment() {
    private var _binding: FragmentSportNewsBinding? = null
    private val binding get() = _binding!!
    private val viewModel: NewsViewModel by viewModels()
    private val newsAdapter: NewsAdapter by lazy { NewsAdapter() }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSportNewsBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        setRecycle()
        observe()
        loadSateListener(requireContext(), newsAdapter, binding.progressBar,)
    }

    private fun observe() {
        viewModel.sportNews.observe(viewLifecycleOwner, {
            newsAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        })
    }

    private fun setRecycle() {
        binding.rvSportNews.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = newsAdapter
        }
        openArticle()
    }

    private fun openArticle() {
        newsAdapter.setOnNewsClick {
            val action = NewsFragmentDirections.actionNewsFragmentToArticleFragment(it,it.source!!.name)
            Navigation.findNavController(binding.root).navigate(action)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}