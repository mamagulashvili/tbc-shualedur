package com.example.tbcshualedur.ui.extensions

import android.app.Dialog
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ProgressBar
import androidx.appcompat.widget.Toolbar
import androidx.viewbinding.ViewBinding
import com.google.android.material.bottomnavigation.BottomNavigationView

fun ProgressBar.showProgressBar() {
    this.visibility = View.VISIBLE
}

fun ProgressBar.hideProgressBar() {
    this.visibility = View.INVISIBLE
}

fun BottomNavigationView.hide() {
    this.visibility = View.GONE
}

fun BottomNavigationView.show() {
    this.visibility = View.VISIBLE
}

fun Toolbar.hide() {
    this.visibility = View.GONE
}
fun Toolbar.show() {
    this.visibility = View.VISIBLE
}

fun <T> Dialog.setDialog(binding: T, bgColor: Int) {
    binding as ViewBinding
    window!!.setBackgroundDrawableResource(bgColor)
    window!!.requestFeature(Window.FEATURE_NO_TITLE)
    val params = this.window!!.attributes
    params.width = WindowManager.LayoutParams.MATCH_PARENT
    params.height = WindowManager.LayoutParams.WRAP_CONTENT
    setContentView(binding.root)
    this.show()
}
