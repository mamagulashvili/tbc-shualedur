package com.example.tbcshualedur.ui.fragments.newscategories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tbcshualedur.adapter.NewsAdapter
import com.example.tbcshualedur.databinding.FragmentAllNewsBinding
import com.example.tbcshualedur.ui.fragments.news.NewsFragmentDirections
import com.example.tbcshualedur.ui.fragments.news.NewsViewModel
import com.example.tbcshualedur.ui.loadSateListener


class AllNewsFragment : Fragment() {
    private var _binding: FragmentAllNewsBinding? = null
    private val binding get() = _binding!!

    private val viewModel: NewsViewModel by viewModels()
    private val newsAdapter: NewsAdapter by lazy { NewsAdapter() }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAllNewsBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        setRecycle()
        observeData()
        loadSateListener(requireContext(), newsAdapter, binding.progressBar)
    }

    private fun observeData() {
        viewModel.allNews.observe(viewLifecycleOwner, {
            newsAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        })
    }
    private fun setRecycle() {
        binding.rvAllNews.apply {
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
            adapter = newsAdapter
        }
        openArticle()
    }

    private fun openArticle() {
        newsAdapter.setOnNewsClick {
            val action = NewsFragmentDirections.actionNewsFragmentToArticleFragment(it,it.source!!.name)
            Navigation.findNavController(binding.root).navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}