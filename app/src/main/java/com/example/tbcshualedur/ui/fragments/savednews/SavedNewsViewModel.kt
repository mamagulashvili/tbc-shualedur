package com.example.tbcshualedur.ui.fragments.savednews

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tbcshualedur.db.Database
import com.example.tbcshualedur.model.news.Article
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SavedNewsViewModel : ViewModel() {
    fun insertNewsToDb(article: Article) = viewModelScope.launch(Dispatchers.IO) {
        Database.db.newsDao().insertNews(article)
    }

    fun getAllSavedNews() = Database.db.newsDao().getAllSavedNews()

    fun deleteNews(article: Article) = viewModelScope.launch(Dispatchers.IO) {
        Database.db.newsDao().deleteNews(article)
    }

    fun searchNews(query: String) = Database.db.newsDao().searchNews(query)
}