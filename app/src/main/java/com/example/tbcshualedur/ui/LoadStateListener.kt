package com.example.tbcshualedur.ui

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.widget.ProgressBar
import androidx.paging.LoadState
import androidx.paging.PagingDataAdapter
import com.example.tbcshualedur.databinding.DialogErrorBinding
import com.example.tbcshualedur.ui.extensions.hideProgressBar
import com.example.tbcshualedur.ui.extensions.setDialog
import com.example.tbcshualedur.ui.extensions.showProgressBar

fun loadSateListener(
    context: Context,
    adapter: PagingDataAdapter<*, *>,
    progressBar: ProgressBar,
) {
    adapter.addLoadStateListener { state ->
        when (state.source.refresh) {
            is LoadState.Loading -> progressBar.showProgressBar()
            is LoadState.NotLoading -> {
                progressBar.hideProgressBar()
            }
            is LoadState.Error -> showErrorDialog(context, adapter)
            else -> progressBar.hideProgressBar()
        }
    }
}

private fun showErrorDialog(context: Context, adapter: PagingDataAdapter<*, *>) {
    val dialog = Dialog(context)
    val dialogBinding = DialogErrorBinding.inflate(LayoutInflater.from(context))
    dialog.setDialog(dialogBinding, android.R.color.transparent)
    dialogBinding.apply {
        tvError.text = "Error, Press Retry"
        btnRetry.setOnClickListener {
            adapter.retry()
            dialog.hide()
        }
    }
}