package com.example.tbcshualedur.ui.bindingadapter

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("imageFromUrl")
fun AppCompatImageView.imageFromUrl(imageUrl:String){
    Glide.with(this.context).load(imageUrl).into(this)
}