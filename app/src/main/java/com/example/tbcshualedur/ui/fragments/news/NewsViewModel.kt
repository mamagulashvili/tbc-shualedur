package com.example.tbcshualedur.ui.fragments.news

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.liveData
import com.example.tbcshualedur.pageresources.NewsPageSource
import com.example.tbcshualedur.util.ResponseHandler

class NewsViewModel : ViewModel() {
    private val responseHandler: ResponseHandler by lazy { ResponseHandler() }

    var allNews = Pager(PagingConfig(pageSize = 10)) {
        NewsPageSource("general")
    }.liveData.cachedIn(viewModelScope)
    var sportNews = Pager(PagingConfig(pageSize = 10)) {
        NewsPageSource("sport")
    }.liveData.cachedIn(viewModelScope)
    var techNews = Pager(PagingConfig(pageSize = 10)) {
        NewsPageSource("technology")
    }.liveData.cachedIn(viewModelScope)
    var businessNews = Pager(PagingConfig(pageSize = 10)) {
        NewsPageSource("business")
    }.liveData.cachedIn(viewModelScope)
    var scienceNews = Pager(PagingConfig(pageSize = 10)) {
        NewsPageSource("science")
    }.liveData.cachedIn(viewModelScope)

}