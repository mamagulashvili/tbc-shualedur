package com.example.tbcshualedur.ui.fragments.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.tbcshualedur.R
import com.example.tbcshualedur.databinding.AccountFragmentBinding
import com.example.tbcshualedur.model.usermodel.UserModel
import com.example.tbcshualedur.ui.extensions.hide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class AccountFragment : Fragment() {

    private var _binding: AccountFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = AccountFragmentBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        val toolbar = activity?.findViewById(R.id.toolbar) as Toolbar
        toolbar.hide()
        auth = Firebase.auth
        getData()
        setProfilePicture()
        binding.btnLogOut.setOnClickListener {
            auth.signOut()
            findNavController().navigate(R.id.action_accountFragment_to_loginFragment)
        }
    }

    private fun getData() = viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
        val currentUserId = auth.currentUser?.uid
        val db = FirebaseFirestore.getInstance()
        val querySnapshot = db.collection("users").document(currentUserId.toString())
            .get().await()
        val email = querySnapshot.data?.getValue("email")
        val userName = querySnapshot.data?.getValue("user_name")
        val password = querySnapshot.data?.getValue("password")
        val user = UserModel(email.toString(), userName.toString(), password.toString())
        binding.user = user
    }

    private fun setProfilePicture() {
        val imageUrlList = mutableListOf(
            "https://www.kindpng.com/picc/m/78-786207_user-avatar-png-user-avatar-icon-png-transparent.png",
            "https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png",
            "https://www.shareicon.net/data/512x512/2016/05/24/770137_man_512x512.png",
        )
        val imageUrl = imageUrlList.random()
        Glide.with(requireContext()).load(imageUrl).into(binding.ivProfile)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}