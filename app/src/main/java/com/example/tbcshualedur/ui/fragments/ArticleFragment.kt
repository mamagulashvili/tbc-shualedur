package com.example.tbcshualedur.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.tbcshualedur.R
import com.example.tbcshualedur.databinding.FragmentArticleBinding
import com.example.tbcshualedur.ui.extensions.hide
import com.example.tbcshualedur.ui.extensions.show
import com.example.tbcshualedur.ui.fragments.savednews.SavedNewsViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView

class ArticleFragment : Fragment() {
    private var _binding: FragmentArticleBinding? = null
    private val binding get() = _binding!!
    private val args: ArticleFragmentArgs by navArgs()
    private val viewModel: SavedNewsViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentArticleBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        val navBar = activity?.findViewById(R.id.bottomNavView) as BottomNavigationView
        navBar.hide()
        val toolbar = activity?.findViewById(R.id.toolbar) as Toolbar
        toolbar.show()
        val article = args.article
        binding.webView.apply {
            webViewClient = WebViewClient()
            article.url?.let { loadUrl(it) }
        }
        binding.btnSave.setOnClickListener {
            viewModel.insertNewsToDb(article)
            findNavController().navigate(R.id.action_articleFragment_to_newsFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}