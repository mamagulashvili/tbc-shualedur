package com.example.tbcshualedur.ui.fragments.auth

import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.tbcshualedur.R
import com.example.tbcshualedur.databinding.LoginFragmentBinding
import com.example.tbcshualedur.ui.extensions.hide
import com.example.tbcshualedur.ui.extensions.hideProgressBar
import com.example.tbcshualedur.ui.extensions.showProgressBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class LoginFragment : Fragment() {
    private var _binding: LoginFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var auth: FirebaseAuth
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LoginFragmentBinding.inflate(layoutInflater, container, false)
        init()

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val user = Firebase.auth.currentUser
        if (user != null) {
            findNavController().navigate(R.id.action_loginFragment_to_newsFragment)
        }
    }

    private fun init() {
        val navBar = activity?.findViewById(R.id.bottomNavView) as BottomNavigationView
        navBar.hide()

        auth = Firebase.auth

        validUserInputs()
        setBackgroundAnim()
        binding.btnLogIn.setOnClickListener {
            logIn()
        }
        binding.btnOpenSignUpFragment.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registrationFragment)
        }
    }

    private fun logIn() {
        val emailAddress = binding.etEmail.text.toString()
        val password = binding.etPassword.text.toString()
        if (emailAddress.isNotEmpty() && password.isNotEmpty()) {
            if (isValidEmail(emailAddress)) {
                binding.progressBar.showProgressBar()
                viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
                    try {
                        auth.signInWithEmailAndPassword(emailAddress, password).await()
                        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
                            setSnackBar("Successfully Log In", Color.GREEN)
                            findNavController().navigate(R.id.action_loginFragment_to_newsFragment)
                        }

                    } catch (e: Exception) {
                        Log.d("ERROR", "$e")
                        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
                            setSnackBar("Failed Log In", Color.RED)
                            binding.progressBar.hideProgressBar()
                        }
                    }
                }
            } else {
                setSnackBar("InCorrect Email Address", Color.RED)
            }

        } else {
            setSnackBar("Please Fill All Field!", Color.RED)
        }
    }

    private fun validUserInputs() {
        binding.etEmail.doOnTextChanged { text, _, _, _ ->
            if (!text?.let { isValidEmail(it) }!!) {
                binding.containerEmail.error = "InCorrect Email"
            } else {
                binding.containerEmail.error = null
            }
        }
    }

    private fun isValidEmail(email: CharSequence): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun setSnackBar(message: String, color: Int) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_SHORT).apply {
            setTextColor(color)
        }.show()
    }

    private fun setBackgroundAnim() {
        val drawableAnim: AnimationDrawable = binding.root.background as AnimationDrawable
        drawableAnim.apply {
            setEnterFadeDuration(3000)
            setExitFadeDuration(1500)
            start()
        }
    }
}