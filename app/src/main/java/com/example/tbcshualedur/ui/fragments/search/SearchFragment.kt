package com.example.tbcshualedur.ui.fragments.search

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tbcshualedur.R
import com.example.tbcshualedur.adapter.NewsAdapter
import com.example.tbcshualedur.databinding.SearchFragmentBinding
import com.example.tbcshualedur.ui.extensions.show
import com.example.tbcshualedur.ui.loadSateListener

class SearchFragment : Fragment() {
    private var _binding: SearchFragmentBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SearchViewModel by viewModels()
    private val newsAdapter: NewsAdapter by lazy { NewsAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SearchFragmentBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        val toolbar = activity?.findViewById(R.id.toolbar) as Toolbar
        toolbar.show()
        setHasOptionsMenu(true)
        setRecycle()
        observe()
        loadSateListener(requireContext(), newsAdapter, binding.progressBar)
        handleEmptySearch()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_search, menu)

        val searchItem = menu.findItem(R.id.menu_search)
        val searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    binding.rvSearchNews.scrollToPosition(0)
                    viewModel.searchNews(query)
                    binding.tvEmpty.isVisible = false
                    searchView.clearFocus()
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }

        })
    }

    private fun observe() {
        viewModel.searchNews.observe(viewLifecycleOwner, {
            newsAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        })
    }

    private fun setRecycle() {
        binding.rvSearchNews.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = newsAdapter
        }
        openArticle()
    }

    private fun handleEmptySearch() {
        newsAdapter.addLoadStateListener { state ->
            binding.tvEmpty.isVisible =
                state.source.refresh is LoadState.NotLoading && state.append.endOfPaginationReached && newsAdapter.itemCount < 1
        }
    }

    private fun openArticle() {
        newsAdapter.setOnNewsClick {
            val action =
                SearchFragmentDirections.actionSearchFragmentToArticleFragment(it, it.source!!.name)
            Navigation.findNavController(binding.root).navigate(action)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}