package com.example.tbcshualedur.ui.fragments.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.example.tbcshualedur.R
import com.example.tbcshualedur.adapter.ViewPagerAdapter
import com.example.tbcshualedur.databinding.NewsFragmentBinding
import com.example.tbcshualedur.ui.extensions.hide
import com.example.tbcshualedur.ui.extensions.show
import com.example.tbcshualedur.ui.fragments.newscategories.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayoutMediator

class NewsFragment : Fragment() {
    private var _binding: NewsFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var pagerAdapter: ViewPagerAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = NewsFragmentBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        val navBar = activity?.findViewById(R.id.bottomNavView) as BottomNavigationView
        navBar.show()
        val toolbar = activity?.findViewById(R.id.toolbar) as Toolbar
        toolbar.hide()
        setTabLayout()
    }


    private fun setTabLayout() {
        val fragmentList = mutableListOf(
            AllNewsFragment(),
            SportNewsFragment(),
            TechNewsFragment(),
            BusinessNewsFragment(),
            ScienceNewsFragment()
        )
        pagerAdapter = ViewPagerAdapter(fragmentList, childFragmentManager, lifecycle)
        binding.viewPager.adapter = pagerAdapter
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            when (position) {
                0 -> tab.text = "All"
                1 -> tab.text = "Sport"
                2 -> tab.text = "Tech"
                3 -> tab.text = "Business"
                4 -> tab.text = "Science"

            }
        }.attach()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}