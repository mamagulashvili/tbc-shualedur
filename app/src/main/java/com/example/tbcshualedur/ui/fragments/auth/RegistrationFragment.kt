package com.example.tbcshualedur.ui.fragments.auth

import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log.d
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.tbcshualedur.R
import com.example.tbcshualedur.databinding.FragmentRegistrationBinding
import com.example.tbcshualedur.ui.extensions.hideProgressBar
import com.example.tbcshualedur.ui.extensions.showProgressBar
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class RegistrationFragment : Fragment() {
    private var _binding: FragmentRegistrationBinding? = null
    private val binding get() = _binding!!

    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegistrationBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        auth = Firebase.auth
        validateUserInputs()

        binding.btnSignUp.setOnClickListener {
            signUp()
        }
    }

    private fun signUp() {
        val emailAddress = binding.etEmail.text.toString()
        val userName = binding.etUserName.text.toString()
        val password = binding.etPassword.text.toString()
        val repeatedPassword = binding.etRepeatPassword.text.toString()
        if (emailAddress.isNotEmpty() && userName.isNotEmpty() && password.isNotEmpty() && repeatedPassword.isNotEmpty()) {
            if (password == repeatedPassword) {
                if (isValidEmail(emailAddress)) {
                    binding.progressBar.showProgressBar()
                    viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
                        try {
                            auth.createUserWithEmailAndPassword(emailAddress, repeatedPassword)
                                .await()
                            viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
                                setSnackBar("Successfully Sign Up", Color.GREEN)
                            }
                            val uid = auth.currentUser?.uid
                            insertUserDataToRealTimeDatabase(
                                uid.toString(),
                                emailAddress,
                                userName,
                                password
                            )
                            findNavController().navigate(R.id.action_registrationFragment_to_loginFragment)
                        } catch (e: Exception) {
                            d("ERROR", "$e")
                            viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
                                setSnackBar("Failed Sign Up", Color.RED)
                            }
                            binding.progressBar.hideProgressBar()
                        }
                    }
                } else {
                    setSnackBar("InCorrect Email Address", Color.RED)
                }

            } else {
                setSnackBar("Please Repeat Password!", Color.RED)
            }
        } else {
            setSnackBar("Please Fill All Field!", Color.RED)
        }
    }

    private fun insertUserDataToRealTimeDatabase(
        uid: String,
        emailAddress: String,
        userName: String,
        password: String
    ) {
        val db = FirebaseFirestore.getInstance()
        val user: MutableMap<String, Any> = HashMap()
        user["uid"] = uid
        user["email"] = emailAddress
        user["user_name"] = userName
        user["password"] = password

        db.collection("users").document(auth.currentUser?.uid.toString()).set(user)
            .addOnSuccessListener {
                d("FIRESTORE", "Successfully")
            }
    }

    private fun validateUserInputs() {
        binding.etEmail.doOnTextChanged { text, _, _, _ ->
            if (!text?.let { isValidEmail(it) }!!) {
                binding.containerEmail.error = "InCorrect Email"
            } else {
                binding.containerEmail.error = null
            }
        }
    }

    private fun isValidEmail(email: CharSequence): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun setSnackBar(message: String, color: Int) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_SHORT).apply {
            setTextColor(color)
        }.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}