package com.example.tbcshualedur.ui.fragments.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.liveData
import com.example.tbcshualedur.pageresources.SearchNewsSource

class SearchViewModel : ViewModel() {

    private val currentQuery = MutableLiveData("")
    val searchNews = currentQuery.switchMap { query ->
        search(query).cachedIn(viewModelScope)
    }
    fun searchNews(query:String){
        currentQuery.value = query
    }

    private fun search(query:String) = Pager(
        PagingConfig(10)
    ){
        SearchNewsSource(query)
    }.liveData

}