package com.example.tbcshualedur.ui.fragments.savednews

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcshualedur.R
import com.example.tbcshualedur.adapter.SavedNewsAdapter
import com.example.tbcshualedur.databinding.SavedNewsFragmentBinding
import com.example.tbcshualedur.ui.extensions.show
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator

class SavedNewsFragment : Fragment() {
    private var _binding: SavedNewsFragmentBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SavedNewsViewModel by viewModels()
    private val savedNewsAdapter: SavedNewsAdapter by lazy { SavedNewsAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SavedNewsFragmentBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        val navBar = activity?.findViewById(R.id.bottomNavView) as BottomNavigationView
        navBar.show()
        val toolbar = activity?.findViewById(R.id.toolbar) as Toolbar
        toolbar.show()
        setRecycle()
        observe()
        swipeDelete()
        setHasOptionsMenu(true)
    }

    private fun observe() {
        viewModel.getAllSavedNews().observe(viewLifecycleOwner, {
            savedNewsAdapter.setData(it.toMutableList())
        })
    }

    private fun setRecycle() {
        binding.rvSavedNews.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = savedNewsAdapter
            itemAnimator = SlideInUpAnimator().apply {
                addDuration = 300
            }
        }
        openArticle()
    }

    private fun openArticle() {
        savedNewsAdapter.setOnSavedNewsClick {
            val action = SavedNewsFragmentDirections.actionSavedNewsFragmentToArticleFragment(
                it,
                it.source!!.name
            )
            Navigation.findNavController(binding.root).navigate(action)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_search, menu)
        val searchItem = menu.findItem(R.id.menu_search)
        val searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    searchNews(query)
                    searchView.clearFocus()
                }
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                if (query != null) {
                    searchNews(query)
                }
                return true
            }

        })
    }

    private fun searchNews(query: String) {
        val searchQuery = "%$query%"
        viewModel.searchNews(searchQuery).observe(viewLifecycleOwner, {
            it.let {
                savedNewsAdapter.setData(it.toMutableList())
            }
        })
    }

    private fun swipeDelete() {
        val itemTouch = object : ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.absoluteAdapterPosition
                val news = savedNewsAdapter.newsList[position]
                viewModel.deleteNews(news)
                Snackbar.make(binding.root, "Successfully Deleted", Snackbar.LENGTH_SHORT).apply {
                    setAction("Undo") {
                        viewModel.insertNewsToDb(news)
                    }.show()
                }
            }

        }
        ItemTouchHelper(itemTouch).apply {
            attachToRecyclerView(binding.rvSavedNews)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}