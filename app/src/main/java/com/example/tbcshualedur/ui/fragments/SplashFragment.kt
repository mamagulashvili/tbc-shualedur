package com.example.tbcshualedur.ui.fragments

import android.app.Dialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.*
import android.net.NetworkCapabilities.*
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.tbcshualedur.R
import com.example.tbcshualedur.databinding.DialogErrorBinding
import com.example.tbcshualedur.databinding.FragmentSplashBinding
import com.example.tbcshualedur.ui.extensions.hide
import com.example.tbcshualedur.ui.extensions.setDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashFragment : Fragment() {
    private var _binding: FragmentSplashBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSplashBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        val navBar = activity?.findViewById(R.id.bottomNavView) as BottomNavigationView
        navBar.hide()
        val toolbar = activity?.findViewById(R.id.toolbar) as Toolbar
        toolbar.hide()
        openLoginFragment()
    }

    private fun openLoginFragment() {
        if (hasInternetConnection()) {
            viewLifecycleOwner.lifecycleScope.launch {
                delay(2500)
                findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
            }
        } else {
            showDialog()
        }
        binding.root.setOnClickListener {
            findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
        }
    }

    private fun showDialog() {
        val dialog = Dialog(requireContext())
        val dialogBinding = DialogErrorBinding.inflate(layoutInflater)
        dialog.setDialog(dialogBinding, android.R.color.transparent)
        dialogBinding.tvError.text = "Internet Connection Error"
        dialogBinding.btnRetry.setOnClickListener {
            openLoginFragment()
            dialog.hide()
        }

    }

    private fun hasInternetConnection(): Boolean {
        val connectionManager = context?.getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val activeNetwork = connectionManager.activeNetwork ?: return false
            val capabilities =
                connectionManager.getNetworkCapabilities(activeNetwork) ?: return false
            return when {
                capabilities.hasTransport(TRANSPORT_WIFI) -> true
                capabilities.hasTransport(TRANSPORT_CELLULAR) -> true
                capabilities.hasTransport(TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectionManager.activeNetworkInfo?.run {
                return when (type) {
                    TYPE_WIFI -> true
                    TYPE_ETHERNET -> true
                    TYPE_MOBILE -> true
                    else -> false
                }
            }
        }
        return false
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}