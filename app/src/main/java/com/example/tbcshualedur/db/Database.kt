package com.example.tbcshualedur.db

import androidx.room.Room
import com.example.tbcshualedur.ui.BaseApplication
import com.example.tbcshualedur.util.Constants.DATABASE_NAME

object Database {

    val db: NewsDatabase by lazy {
        Room.databaseBuilder(
            BaseApplication.context,
            NewsDatabase::class.java,
            DATABASE_NAME
        ).build()
    }
}