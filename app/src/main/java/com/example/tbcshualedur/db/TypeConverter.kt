package com.example.tbcshualedur.db

import androidx.room.TypeConverter
import com.example.tbcshualedur.model.news.Source

class TypeConverter {
    @TypeConverter
    fun sourceToString(source: Source): String {
        return source.name
    }
    @TypeConverter
    fun stringToSource(name: String): Source {
        return Source("", name)
    }
}