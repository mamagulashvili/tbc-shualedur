package com.example.tbcshualedur.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.tbcshualedur.model.news.Article

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNews(article: Article)

    @Query("SELECT * FROM news_table")
    fun getAllSavedNews(): LiveData<List<Article>>

    @Delete
    suspend fun deleteNews(article: Article)

    @Query("SELECT * FROM news_table WHERE title LIKE :query")
    fun searchNews(query: String): LiveData<List<Article>>
}