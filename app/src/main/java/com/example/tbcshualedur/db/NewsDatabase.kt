package com.example.tbcshualedur.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.tbcshualedur.model.news.Article

@Database(entities = [Article::class], version = 1,exportSchema = false)
@TypeConverters(TypeConverter::class)
abstract class NewsDatabase : RoomDatabase() {
    abstract fun newsDao(): NewsDao
}