package com.example.tbcshualedur.model.news

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

data class NewsResponse<T>(
    val articles: T?,
    val status: String?,
    val totalResults: Int?
)

@Entity(tableName = "news_table")
data class Article(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val author: String?,
    val content: String?,
    val description: String?,
    val publishedAt: String?,
    val title: String?,
    val url: String?,
    val urlToImage: String?,
    val source: Source?
) : Serializable

data class Source(
    val id: Any,
    val name: String
)