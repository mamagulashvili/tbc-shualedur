package com.example.tbcshualedur.model.usermodel

data class UserModel(
    val email: String? = null,
    val userName: String? = null,
    val password: String? = null
)
