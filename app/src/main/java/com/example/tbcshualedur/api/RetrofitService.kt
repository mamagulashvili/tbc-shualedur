package com.example.tbcshualedur.api

import com.example.tbcshualedur.util.Constants.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {

    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    val newsApi: NewsApi by lazy {
        retrofit.create(NewsApi::class.java)
    }
}