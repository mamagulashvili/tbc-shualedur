package com.example.tbcshualedur.api

import com.example.tbcshualedur.model.news.Article
import com.example.tbcshualedur.model.news.NewsResponse
import com.example.tbcshualedur.util.Constants.API_KEY
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {
    @GET("v2/top-headlines")
    suspend fun getNews(
        @Query("country")
        country: String = "gb",
        @Query("category")
        category: String,
        @Query("page")
        page: Int,
        @Query("apiKey")
        apiKey: String = API_KEY
    ): Response<NewsResponse<List<Article>>>

    @GET("v2/everything")
    suspend fun searchNews(
        @Query("q")
        query: String,
        @Query("page")
        page: Int,
        @Query("apiKey")
        apiKey: String = API_KEY
    ): Response<NewsResponse<List<Article>>>
}