package com.example.tbcshualedur.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcshualedur.databinding.RowItemBinding
import com.example.tbcshualedur.model.news.Article

class SavedNewsAdapter : RecyclerView.Adapter<SavedNewsAdapter.SavedNewsViewHolder>() {

    var newsList = mutableListOf<Article>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedNewsViewHolder {
        return SavedNewsViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SavedNewsViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int = newsList.size

    inner class SavedNewsViewHolder(val binding: RowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            val news = newsList[absoluteAdapterPosition]
            binding.news = news
            itemView.setOnClickListener {
                onSaveNewsClick?.let { it(news) }
            }
        }
    }

    fun setData(list: MutableList<Article>) {
        this.newsList = list
        notifyDataSetChanged()
    }

    private var onSaveNewsClick: ((Article) -> Unit)? = null

    fun setOnSavedNewsClick(listener: (Article) -> Unit) {
        onSaveNewsClick = listener
    }

}