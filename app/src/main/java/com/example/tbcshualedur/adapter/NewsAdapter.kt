package com.example.tbcshualedur.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcshualedur.databinding.RowItemBinding
import com.example.tbcshualedur.model.news.Article

class NewsAdapter : PagingDataAdapter<Article, NewsAdapter.NewsViewHolder>(Differ) {

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.onBind()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    inner class NewsViewHolder(val binding: RowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            val news = getItem(absoluteAdapterPosition)
            binding.news = news
            itemView.setOnClickListener {
                onNewsSelected?.let {
                    if (news != null) {
                        it(news)
                    }
                }
            }
        }
    }

    object Differ : DiffUtil.ItemCallback<Article>() {
        override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem.url == newItem.url
        }

        override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem == newItem
        }
    }
    private var onNewsSelected:((Article) -> Unit)? = null

    fun setOnNewsClick(listener:(Article) -> Unit){
        onNewsSelected = listener
    }
}